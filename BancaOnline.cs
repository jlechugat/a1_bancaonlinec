using System;

class MainClass {
  //strings d'usuaris, contras i el nom.
  public static string[] usersguays = {"prueba","56874526L",""};
  public static string[] contrasguays = {"123","321",""};
  public static string[] Nicks = {"Jorge","Angel",""};
  //diners 
  public static float[] saldo = {6969, 674, 0};
  //Tonterias varias quan consultes
  public static string tonteriesmilio = "WOW!!!! comprat un tesla que son molt guais";
  public static string[] tonteries10000 = {"ja et pots comprar un bitcoin", "comprat una Gigabyte AORUS GeForce RTX 3080 XTREME 10GB GDDR6X "};
  public static string tonteries500 = "has de pagar el lloguer, para de gastar en tonteries!";
  public static string tonteries10 = "estas al pozo, no tens ni per un mcdonalds";
  public static string tonterieszero ="ens deus diners, paga o rebras";


//MAIN
  public static void Main () {
    MenuPrincipal();
  }
//Menu Principal 
  public static void MenuPrincipal(){
    Console.WriteLine ("Welcome to the online Banc\n\nQue vols fer?\n\n");
    Console.WriteLine ("1.Iniciar sessió\n2.Registrarte\n3.Sortir");
    string eleccio;
    eleccio = Console.ReadLine();
    switch (eleccio)
    {
      case "1": SignIn();
        break;
      case "2": Register();
        break;
      case "3": Console.WriteLine ("Adeu");
        break;
      default: Console.WriteLine("que haces loco");
               break;
    }
  }
//Iniciar sessió amb control de validació
  public static void SignIn(){
    string user;
    string contra;
    bool usercheck = false;
    bool contracheck = false;
    bool validated = false;

    while (validated == false){
      Console.WriteLine("Pa iniciar sesion dime tu DNI");
      user = Console.ReadLine();
      Console.WriteLine("Ahora dime la contra");
      contra = Console.ReadLine();

      if (Array.IndexOf(usersguays, user) >= 0){
        int indexdeluser = Array.IndexOf(usersguays, user);
        usercheck = true;
        if (Array.IndexOf(contrasguays, contra) == indexdeluser){
          contracheck = true;
          if (usercheck == true && contracheck == true){
            validated = true;
            MenuOperaciones(indexdeluser);
          }else{
            Console.WriteLine("Qui ets");
          }
        }else{
          usercheck = false;
          Console.WriteLine("Qui ets");
        }
      }else{ 
        Console.WriteLine("Qui ets");
      }
    }
  }
  //Menú amb la sessio iniciada.
  public static void MenuOperaciones(int quien){
    Console.WriteLine("\nBenvingut " + Nicks[quien] + "\n\nQue vols fer?\n\n");
    Console.WriteLine("1. Posar diners\n2. Treure diners\n3. Consultar\n4. Sortir\n");
    string eleccio;
    eleccio = Console.ReadLine();
    switch (eleccio)
    {
      case "1": saldo[quien] = Ingressar(saldo[quien]);
        MenuOperaciones(quien); 
        break;
      case "2": saldo[quien] = Treure(saldo[quien]);
        MenuOperaciones(quien); 
        break;
      case "3": saldo[quien] = Consultar(saldo[quien]);
        MenuOperaciones(quien); 
        break;
      case "4": Console.WriteLine ("Adeu");
        MenuPrincipal();
        break;
      default: Console.WriteLine("que haces loco");
        break;
    }
  }
  //L'usuari es pot registrar si hi ha un lloc disponible.
  public static void Register(){
    if (Array.IndexOf(usersguays, "") >= 0){
      int hueco = Array.IndexOf(usersguays, "");
  
      Console.WriteLine("\n\nQuin sera el teu DNI?");
      usersguays[hueco] = Console.ReadLine();
      Console.WriteLine("\n\nQuina sera la teva contrasenya?");
      contrasguays[hueco] = Console.ReadLine();
      Console.WriteLine("\n\nCom vols que et diguin?");
      Nicks[hueco] = Console.ReadLine();
    }else{
      Console.WriteLine("\n\nNo hi han llocs disponibles. :(");
    }
    
    MenuPrincipal();
  }
  //Funcio d'ingressar diners amb control de blanqueig
  public static float Ingressar(float saldo){
    float euros;
    Console.WriteLine("\nQuant vas a ingressar? Recorda que nomes es poden ingressar 1000 cada vegada.");
    euros=Convert.ToSingle(Console.ReadLine());
    if (euros>1000 && euros<1){
      Console.WriteLine("\n\nAixo no es una xifra valida, no pots blanquejar diners ni ingressar nombres negatius crack.");
      return saldo;
    }else{
      saldo += euros;
      return saldo;
    }

  }
  //Funcio de treure diners amb control de no treure coses dolentes.
  public static float Treure(float saldo){
    float euros;
    Console.WriteLine("\nQuant vols treure?");
    euros=Convert.ToSingle(Console.ReadLine());
    if (euros>0 && euros<=saldo*2){
      saldo -= euros;
      return saldo;
    }else{
      Console.WriteLine("\nIntrodueix una xifra valida i assegurat que tens diners.");
      return saldo;
    }
  }
  //Funcio consultar amb les tonteries dels diners i els prestamos.
  public static float Consultar(float saldo){
    Console.WriteLine("\n"+"Ara mateix tens "+saldo+"€");
    if(saldo<0 && saldo>-1000){
      Console.WriteLine("\n"+tonterieszero+"\n");
      return saldo;
    }else if(saldo<=-1000){
      saldo = Prestamo(saldo);
      Console.WriteLine("\n Ara tens " + saldo + " que guai.");
      return saldo;
    }else if(saldo<=10 && saldo>0){
      Console.WriteLine("\n"+tonteries10+"\n");
      return saldo;
    }else if(saldo>10 && saldo<=500){
      Console.WriteLine("\n"+tonteries500+"\n");
      return saldo;
    }else if(saldo>500 && saldo<=10000){
      Random rnd = new Random();
      int randomnumber = rnd.Next(0, 2);  // creates a number between 0 and 1
      Console.WriteLine("\n"+tonteries10000[randomnumber]+"\n");
      return saldo;
    }else if(saldo>10000){
      Console.WriteLine("\n"+tonteriesmilio+"\n");
      return saldo;    
    }else{
      Console.WriteLine("?");
      return saldo;
    }
  }
  //Prestamo dinamico depenent dels diners que tens
  public static float Prestamo(float saldo){
    float opcionprestada;
    opcionprestada = (saldo*-1)*2;
    Console.WriteLine("\n Ens deus " + saldo + "€");
    Console.WriteLine("\nT'oferim un prestamo de " + opcionprestada + "€, que dius? (si o no)");
    string decision = "";
    while (decision !="si" && decision !="no"){
      decision = Console.ReadLine();
      if (decision == "si"){
        saldo = saldo + opcionprestada;
        return saldo;
      }else if(decision == "no"){
        Console.WriteLine("Vale, tu tho perds moroso.");
        return saldo;
      }else{
        Console.WriteLine("Digues si o no loco.");
        return saldo;
      }
    }
    return saldo;
  }
}